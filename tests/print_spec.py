#!/usr/bin/python3
import sys
sys.path.append('lib')
sys.path.append('cndprint')

from mamba import description, context, it
from expects import expect, equal
# import mockito
from cnd_print import CndPrint

with description('CndPrint') as self:
    with before.each:
        self.message = "message !"
        self.cnd_print = CndPrint()

with context('each method should retrun True or False on default'):
    with before.each:
        self.message = "message !"
        self.cnd_print = CndPrint(silent_mode=True)

    with it('log_e should return False'):
        result = self.cnd_print.log_e(self.message)
        expect(result).to(equal(False))

    with it('log_v should return False'):
        result = self.cnd_print.log_v(self.message)
        expect(result).to(equal(False))

    with it('log_d should return False'):
        result = self.cnd_print.log_d(self.message)
        expect(result).to(equal(False))

    with it('trace_e should return False'):
        result = self.cnd_print.trace_e(self.message)
        expect(result).to(equal(False))

    with it('trace_v should return False'):
        result = self.cnd_print.trace_v(self.message)
        expect(result).to(equal(False))

    with it('trace_d should return False'):
        result = self.cnd_print.trace_d(self.message)
        expect(result).to(equal(False))

    with it('info_e should return False'):
        result = self.cnd_print.info_e(self.message)
        expect(result).to(equal(True))

    with it('info_v should return False'):
        result = self.cnd_print.info_v(self.message)
        expect(result).to(equal(True))

    with it('info_d should return False'):
        result = self.cnd_print.info_d(self.message)
        expect(result).to(equal(True))

with context('each method should return True or False on Log mode'):
    with before.each:
        self.message = "message !"
        self.cnd_print = CndPrint("Log", silent_mode=True)

    with it('log_e should return False'):
        result = self.cnd_print.log_e(self.message)
        expect(result).to(equal(True))

    with it('log_v should return False'):
        result = self.cnd_print.log_v(self.message)
        expect(result).to(equal(True))

    with it('log_d should return False'):
        result = self.cnd_print.log_d(self.message)
        expect(result).to(equal(True))

    with it('trace_e should return False'):
        result = self.cnd_print.trace_e(self.message)
        expect(result).to(equal(False))

    with it('trace_v should return False'):
        result = self.cnd_print.trace_v(self.message)
        expect(result).to(equal(False))

    with it('trace_d should return False'):
        result = self.cnd_print.trace_d(self.message)
        expect(result).to(equal(False))

with context('each method should return True or False on Trace mode'):
    with before.each:
        self.message = "message !"
        self.cnd_print = CndPrint("Trace", silent_mode=True)

    with it('log_e should return False'):
        result = self.cnd_print.log_e(self.message)
        expect(result).to(equal(True))

    with it('log_v should return False'):
        result = self.cnd_print.log_v(self.message)
        expect(result).to(equal(True))

    with it('log_d should return False'):
        result = self.cnd_print.log_d(self.message)
        expect(result).to(equal(True))

    with it('trace_e should return False'):
        result = self.cnd_print.trace_e(self.message)
        expect(result).to(equal(True))

    with it('trace_v should return False'):
        result = self.cnd_print.trace_v(self.message)
        expect(result).to(equal(True))

    with it('trace_d should return False'):
        result = self.cnd_print.trace_d(self.message)
        expect(result).to(equal(True))

    with it('should test all color and event'):
        for state in CndPrint.states:
            for color in CndPrint.colors:
                result = getattr(self.cnd_print, f"{state}_{color}")(f"{state}_{color} -> {self.message}")
                expect(result).to(equal(True))

with context('build_message when uuid is not provided'):
    with before.each:
        self.message = "message !"
        self.uuid = "0000"
        self.cnd_print = CndPrint(silent_mode=True)
        self.items = self.cnd_print._CndPrint__build_message(self.message).split(self.cnd_print.separator)

    with it('should include message'):
        expect(self.items[-1]).to(equal(self.message))

    with it('should return 2 items'):
        expect(len(self.items)).to(equal(2))

with context('build_message when uuid is provided'):
    with before.each:
        self.message = "message !"
        self.uuid = "0000"
        self.cnd_print = CndPrint(uuid=self.uuid, silent_mode=True)
        self.items = self.cnd_print._CndPrint__build_message(self.message).split(self.cnd_print.separator)

    with it('should include message'):
        expect(self.items[-1]).to(equal(self.message))

    with it('should include uuid'):
        expect(self.items[0]).to(equal(self.uuid))

    with it('should return 3 items'):
        expect(len(self.items)).to(equal(3))
